package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class StudentServiceImplTest {
   StudentDao studentDao ;
   StudentService studentService;
   @Before
   public void setup(){
    studentDao = mock(StudentDao.class);
    studentService = new StudentServiceImpl();
    ((StudentServiceImpl) studentService).setStudentDao(studentDao);
   }
  @Test
  public void testFindById(){
      StudentDaoImpl studentDao = new StudentDaoImpl();
      StudentServiceImpl studentService = new StudentServiceImpl();
      studentService.setStudentDao(studentDao);
      assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
      assertThat(studentService.findStudentById("789"),is(new Student("789","AAA","ww",1.0)));
      assertThat(studentService.findStudentById("111"),is(new Student("111","AAAA","ee",3.5)));
      assertThat(studentService.findStudentById("222"),is(new Student("222","AAAAA","rr",2.8)));
  }

    @Test
    public void testGetAverageGpa(){
      StudentDaoImpl studentDao = new StudentDaoImpl();
      StudentServiceImpl studentService = new StudentServiceImpl();
      studentService.setStudentDao(studentDao);
      assertThat(studentService.getAverageGpa(),is(2.686));
    }
    @Test
    @Parameters(method = "paramsForAverageParams")
    public void testAverageGpa(List<Student>  students,double average) {
        StudentServiceImpl studentService = new StudentServiceImpl();
        StudentDaoImpl studentDao = new StudentDaoImpl(students);
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(average));
    }
    public Object paramsForAverageParams() {
        List<Student> students;
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",3.4));
        students.add(new Student("456","AA","qq",3.8));
        students.add(new Student("789","AAA","ww",3.5));
        students.add(new Student("111","AAAA","ee",3.5));
        students.add(new Student("222","AAAAA","rr",3.6));
        return new Object[][]{
                {students,3.56}
        };
    }
    @Test
    public void testWithMoc(){
       // StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33) ));
    }
    @Test
    public void testWithMyMoc(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",3.4));
        mockStudents.add(new Student("456","AA","qq",3.8));
        mockStudents.add(new Student("789","AAA","ww",3.5));
        mockStudents.add(new Student("111","AAAA","ee",3.5));
        mockStudents.add(new Student("222","AAAAA","rr",3.6));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.56));
        assertThat(studentService.findStudentByPartOfId("1"),
                hasItem(new Student("123","A","temp",3.4) ));
        assertThat(studentService.findStudentByPartOfId("1"),
                hasItems(new Student("123","A","temp",3.4),new Student("111","AAAA","ee",3.5) ));

        mockStudents.clear();
        mockStudents.add(new Student("789","AAA","ww",3.5));
        mockStudents.add(new Student("111","AAAA","ee",3.5));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.5));
        mockStudents.clear();
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(2.33));

    }
    @Test
    public void testFindByPartOfId(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),
                hasItem(new Student("223","C","temp",2.33) ));
        assertThat(studentService.findStudentByPartOfId("22"),
               hasItems(new Student("223","C","temp",2.33),new Student("224","D","temp",2.33) ));
    }
    @Test(expected = NoDataException.class)
    public void testNoDataException(){
       List<Student> mockStudents = new ArrayList<>();
       mockStudents.add(new Student("123","A","temp",2.33));
       when(studentDao.findAll()).thenReturn(mockStudents);
       assertThat(studentService.findStudentById("12"),nullValue());
    }
    @Test(expected = ArithmeticException.class)
    public void testZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",0.0));
        mockStudents.add(new Student("224","D","temp",0.0));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());
    }
}
