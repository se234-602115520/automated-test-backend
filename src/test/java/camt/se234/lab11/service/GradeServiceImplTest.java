package camt.se234.lab11.service;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest {
    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test getGrade Params [{index}] : input is {0} , expect \"{1}\"")
    public void testGetGradeparams(double score,String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(score),is(expectedGrade));
    }

    public Object paramsForTestGetGradeParams() {
        return new Object[][]{
                {100,"A"},
                {77,"B"},
                {74,"C"},
                {50,"D"},
                {15,"F"}
        };
    }
    @Test
    @Parameters(method = "paramsForGetGradeParams")
    public void testGradeparams(double midterScore,double finalScore,String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(midterScore,finalScore),is(expectedGrade));
    }
    public Object paramsForGetGradeParams() {
        return new Object[][]{
                {100,100,"A"},
                {77,77,"B"},
                {74,74,"C"},
                {50,50,"D"},
                {15,15,"F"}
        };
    }
}
