package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("456","AA","qq",3.8));
        students.add(new Student("789","AAA","ww",1.0));
        students.add(new Student("111","AAAA","ee",3.5));
        students.add(new Student("222","AAAAA","rr",2.8));

    }
    public StudentDaoImpl( List<Student> students){
        this.students=students;
    }
    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
